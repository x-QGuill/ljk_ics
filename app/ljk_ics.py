from bs4 import BeautifulSoup
from ics import Calendar, Event
from requests import get

def get_html():
    """
    Get the HTML of the page
    """
    url = "https://www-ljk.imag.fr/spip.php?article10"
    request = get(url, verify=False)
    return request.text

def main():
    """
    main function
    """
    html_content = get_html()

    soup = BeautifulSoup(html_content, "html.parser")

    calendar = Calendar()

    for event in soup.find_all("li", class_="event"):
        # --- TITLE ---
        title = event.b.a.contents[0]

        # --- AUTHOR ---
        author = event.find("span", class_="auteur")
        if author is not None:
            author = author.contents[1]

        # --- DATE ---
        date = event.find("span", class_="date")
        if date is not None:
            date = date.contents[1]
            splitted_date = date.split(" - ")
            day = splitted_date[0]
            hour = splitted_date[1]
            splitted_day = day.split("/")

            date = f"{splitted_day[2]}-{splitted_day[1]}-{splitted_day[0]} {hour}:00+02"

        # --- PLACE ---
        place = event.find("span", class_="lieu")
        if place is not None:
            place = place.contents[1]

        ics_event = Event()
        ics_event.name = f"{title} by {author}"
        ics_event.begin = date
        ics_event.location = place

        calendar.events.add(ics_event)

        # print(f"Title: {title}, by {author}, @ {date}, {place}")

    with open("ljk.ics", "w") as my_ics:
        my_ics.writelines(calendar)

if __name__ == "__main__":
    main()

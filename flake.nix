{
  description = "A very basic flake";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs { system = "x86_64-linux"; };

  in {

    packages.x86_64-linux.ljk = with pkgs;
      python38Packages.buildPythonPackage rec {
        name = "ljk_ics";
        version = "1.0";
        src = ./.;
        propagatedBuildInputs = with python38Packages; [
            beautifulsoup4
            ics
            requests
        ];
        doCheck = false;
      };

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.ljk;
  };
}

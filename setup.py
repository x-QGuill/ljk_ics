from setuptools import setup

setup(
    # Application name:
    name="ljk_ics",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author="Quentin Guilloteau",
    author_email="Quentin.Guilloteau@inria.fr",

    # Packages
    packages=["app"],

    # Include additional files into the package
    # include_package_data=True,
    entry_points={
        'console_scripts': ['ljk_ics=app.ljk_ics:main'],
    },

    # Details
    url="https://gitlab.inria.fr/qguillot/ljk_ics",

    #
    # license="LICENSE.txt",
    description="Script generating a ics file of the LJK seminars",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
        "beautifulsoup4",
        "ics",
        "requests"
    ]
)
